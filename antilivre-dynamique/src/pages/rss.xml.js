import rss, { pagesGlobToRssItems } from '@astrojs/rss';
import { getCollection } from 'astro:content';
import { SITE } from "../site-config";

export async function get(context) {
  return rss({
    title: SITE.title,
    description: SITE.description,
    site: context.site,
    items: await pagesGlobToRssItems(
      import.meta.glob('./content/**/*.{md,mdx}'),
    ),
  });
}
