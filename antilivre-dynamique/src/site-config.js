export const SITE = {
  author: 'suf marenda & the AAA',
  title: 'Le Problème avec le Skateboard | Antilivre | Abrüpt',
  displayTitle: 'Le Problème avec le Skateboard',
  description: 'Le Problème avec le Skateboard, c’est d’abord de ne jamais avoir pu faire un kickflip au-dessus de la social-démocratie. Élégance du trick : néant. Notre mémoire a beau jeu de se saisir d’une board, sa langue gratte le béton sans s’échapper des streets invariables du même. Labyrinthe d’underground : zéro. Le problème avec le skateboard, c’est ensuite, ad wheeling æternam, de n’être plus que l’image d’une image, le miroitement policé d’une Hostile Architecture™. Les caméras délimitent la zone. Tout glisse sauf le mobilier urbain. Mais fifty-fifty olympique : skateboard & obéissance. Le problème avec le skateboard, c’est de ne pas rêver assez grand. Disruption en roue libre : just do it.',
  url: 'https://www.antilivre.org/le-probleme-avec-le-skateboard/',
  titlelink: 'https://abrupt.cc/suf-marenda/le-probleme-avec-le-skateboard/',
  lang: 'fr',
  locale: 'fr_FR',
  warning: 'warning',
  credits: 'Antitexte&nbsp;: suf marenda<br>Antimonde&nbsp;: the AAA'
};

// // Theme configuration
// export const PAGE_SIZE = 10;
export const THREE_SITE = true;
