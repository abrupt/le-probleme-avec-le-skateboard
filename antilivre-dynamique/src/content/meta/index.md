<div class="text">

Le problème avec le Problème : « Dans l'expression orale relâchée, *problème* est souvent employé
aujourd'hui dans le sens très général de "question, affaire" ou de
"difficulté, incident" : *ça, c'est ton problème* (...) »

</div>

<div class="text">

Du Skateboard, simplement, je n'en suis pas. pas plus que ça.

 

Tellement pas, d'ailleurs, qu'on doit bientôt me le marquer à chaud sur
l'avant-bras.

 

Il me sera pyrogravé « Le-Skate-et-moi-ne-nous-méritons-pas ».

 

Afin d'affermir la véracité du propos, on ajoutera « j'aimais l'un
l'autre ».

Le problème avec le skateboard, c'est que la pyrogravure, c'est un truc
de bonhomme. Ma douleur sera épouvantable. Et l'on plaint aussi les
narines qui se faderont l'odeur boucanée de mon humanité grillée à
sec...

Tout cela pour quoi, au final ?

 

Pour qu'une fois encore, on n'entrave à peine couic à ce que j'aurai
voulu m'écrire !

</div>

<div class="text">

Le problème avec le skateboard, je dirais, c'est que c'en est
désespérant...

Notamment, parce que du bout de mes lèvres, à mon avant-bras meurtri,
tout paraît déjà limpide :

 

1. je crois, moi, que dans le Vrai Beau Bon ciel,

il y a.

je veux dire, « il y a le skateboard ».

je crois qu'il y a un genre de skate-presque-plus-tard, qui se serre
volontiers le *core* du bon côté de nos communs.

Spontanément, je l'appelle beaucoup le « skate des anges » et/ou « futur
chantant de nos demains ».

C'est bien.

C'est bien... même si ce skate-là, il ne vient pas du ciel, même s'il
reste toujours plus matérialiste que le plus bas des ciels lourdingues.

1. Un tel skateboard s'élève en spirales, il se reconfigure
symboliquement, de barbecues véganes en sessions *riot grrrls*.

Ce skate des anges noircit nos paumes comme on en rêve,

*avant que de les plonger, DIY,*

*dans des bétonnières,*

*en bon désordre de marché.*

Bref, de tous mes yeux, et en haut d'un Abrüpt, il y a grâce à lui,

« le skateboard des pirates » !

Si seulement...

</div>

<div class="text">

Car la vraie Vérité Navrante du Skate, elle demeure

presque

navrante (ici-bas, comme dans le ciel...) :

 

à l'heure actuelle, le vrai Skateboard du Problème (vSP) se laisse trop
peu conduire où j'attendrais, wam, que l'on noumène.

2. le vSP est navrant parce qu'il se fait, lui, plutôt ailleurs.

3. Je vais dire où.

3. Whatever, je considèrerai ici, que l'ailleurs ne pose pas de
*problème*. Pas en soi.

4. Une vieille peluche à l'effigie de Jeff G. (un jeune, un mort au
front...)

peut te livrer la clé de ce paradoxe. Je la pose là :

« *The history of skateboarding is so fucking muddy and grey. There is
no black and white. It hurts so many people's feelings and starts too
many problems (...)* »

 

Tu l'entends, ça ?

La peluche le répète presque mécaniquement,

À *mon* adresse.

 

4...lle doit dire vrai. Elle me le doit absolument, parce que le
*muddy* du Skateboard, c'est tout ce qui, trente ans en amont, l'a rendu
accessible à mes *fakies* adolescents. D'ailleurs, ma première board
était d'emblée toute mélangée

(un pro modèle John Warnock Hinckley, artwork *JFA*, estampillé Ronald
Reagan).

4. Ce Skateboard de 1986, je ne savais pas alors la plupart de ses
gestes. À les exécuter, l'attelage de nos formes vivantes m'adressait
des signes... rien moins que troubles.

</div>

<div class="text">

5. Les premiers raids anti-Mytheux n'ont été lancés que bien plus tard,
au début du siècle suivant.

 

5. C'est avec Los Angeles, que tout a commencé.

 

5. Los Angeles... Elle est vaste, cette ville ! (J'aurais dû m'y
attendre), le taux de perte apoétique s'y est révélé effroyablement
élevé.

 

5...l a donc fallu préciser l'objet de l'investigation et retailler la
plupart de mes *process* --- parce qu'une planche de skate, qu'est-ce
que ça représente ? Au maximum dix pouces de large et, allez !,
trente-deux ou trente-trois pour la hauteur...

À côté de Los Angeles...

</div>

<div class="text">

*L'animalité = première forme apoétique du Skateboard, celle à la faveur
de laquelle on peut encore le saisir par la peau flasque du cou, pour
lui interdire d'exercer son emprise sur les êtres et les choses peuplant
son Mytheux naturel.*

</div>

<div class="text">

Le problème avec un skateboard,

C'est que je l'ai croisé.

 

Il participe au séminaire « Perspectives sur l'emploi causé par
l'animal ».

 

Il s'y présente avec l'avant-garde des choses, et puis « de toute la
matière » --- comme on dit à l'époque, dans les milieux gauchiss'.

 

Rien moins que claires, ses revendications articulent vainement défenses
du bien commun et prises de foirure sur l'intérêt général.

 

Le skateboard écoute tout,

Il parle bien, s'exprime trop mal

et, sans s'étendre sur les sujets, il nous les dit tout de même.

 

À table, il a ce trait d'humour : « Je déclare, moi, le repas
indocile. »

Et puis cet autre : « vous savez que, jadis, je me concentrais tellement
sur mon employabilité que j'en oubliais --- mais carrément ! --- de
récupérer mes filles après l'école.

 

Non, elles, ne m'en voulaient pas.

Car mes enfants savaient

que c'était le problème

avec les industries,

ces avaleuses

de mon travail. »

</div>

<div class="text">

Le problème avec le skateboard,

C'est que tout se déroulerait encore

dans l'Antiquité, s'il n'avait, cet impie, déchaîné tout le fond

et la colère des Dieux.

 

L'histoire débute un samedi. Ce jour-là, le skateboard entreprend de
voler le feu.

Son équation nécessite d'optimiser sous contrainte.

 

Bien sûr, il manque de tout, mais ne renonce à rien.

  

Finalement, le skate s'empare de l'eau.

 

Le lundi, la police informe les Dieux que de la terre a disparu,
« également » --- ce qui, en langage policé, signifie « aussi ». Ne
manque plus alors qu'un vol d'air pur pour plonger le divin dans un
parfait malaise.

 

Et, le jeudi, c'est chose faite,

le skateboard a dérobé toutes les bonbonnes d'oxygène stockées sur
l'Olympe.

Finement, les Dieux le chopent : « 1. tu n'as pas fait tes devoirs 2. tu
es parti avec le feu, l'air, et la terre

et puis avec de l'eau.

Alors, que veux-tu,

*dabbar rasek* ! »

Ainsi, comme ça, ils le chassent de l'Antiquité...

 

Après, pour alourdir sa punition, ils le mandatent juste

pour fabriquer un monde.

 

Et c'est ça, le problème avec le skateboard...

c'est qu'il n'y parvient jamais.

 

mais, jamais !

 

vraiment.

</div>

<div class="text">

Le problème avec le skateboard,

C'est qu'il craint de s'endormir

sur sa tête au soleil.

 

Il s'inquiète d'y voir bouillir un restant de matière.

Il se réveille, habité par d'angoisses épouvantables,

Il se fait du *moron*, pour sa cervelle toute blanche.

</div>

<div class="text">

Le skate, au vrai, se préserve de tout,

Sauf d'un peu de vin, et puis de tête de veau,

Sauf d'un peu du vin et de tout le haschisch,

Sauf « d'un homme très célèbre, qui était en même temps, un grand »
mytho.

 

Il se nourrit vainement,

Prend ses repas au fond d'un casque,

Avant que de les rendre, inclinaison penchée,

têtes à l'envers,

filmées du dessous,

depuis ses *trucks*.

 

On ne lui connaît qu'un but : s'installer dans les images en
mou-ve-ment.

 

Le problème, avec le skateboard,

 

C'est qu'il finira,

par se nourrir de fins.

</div>

<div class="text">

Le problème avec le skateboard,

C'est son infime nativité.

 

Et sa gentillesse mise à part.

 

On l'a vu se tenir seul

dans un drugstore (ou un snack bar), alors que ça n'existait plus.

Il venait presque à la rescousse

et de tous mes exemples.

 

Parce qu'il n'avait plus rien,

au fond de sa tétée.

À peine un doute,

mais à tout prix.

</div>

<div class="text">

Le problème avec le skateboard,

C'est qu'il faudrait encore

 

que ça m'inintéresse.

</div>

<div class="text">

Le problème avec le skateboard,

C'est qu'il adore

se sortir dans les boîtes à consommer,

 

il y est fluide, comme pour toujours,

avec Camille et puis Léa, des étudiantes en mé-deu-cine.

 

Le skateboard, il sue, suprême, par tant de pores,

et il se tue par tous les sorts,

en avalant beaucoup de petits liquides calciques.

Il s'équilibre, lui, l'électrolyte

avec des sels, tous positifs.

Sur le dancefloor, il est dragonne.

on lui propose des trous dans la langue : « Tu viens faire des tours
dans mon camion ? »

Mais le skate ne pénètre pas, il est l'objet de ces querelles.

 

Après manip', certains se jettent à ses pieds :

« Voudriez-vous seulement devenir mon *landlord* et mon mari, la chose
propriétaire de toute ma vie ? »

 

Le skate se cale alors la queue au fond d'un pouf,

Il y rédige des procès verbeux,

qu'il adressera tantôt

à ses analysants.

Ici, au vrai, le problème avec le skateboard nous pue déjà tout de la
teub :

 

*et* il enterre sa vie de jeune fille, *et* on le bombarde

« agent apoétique »,

comme du maussade.

Rien de paisible.

</div>

<div class="text">

Le problème avec le skateboard,

 

C'est qu'il a ricané pendant toute la durée du cours

 

et que vous ne croirez jamais,

jamais,

 

ni quoi.

</div>

<div class="text">

Le problème avec le skateboard,

C'est qu'il faudrait lui tendre tes muscles, et pour souffler.

Il s'en faudrait de beaucoup, par pratiquants de la pratique,

dénaturés.

 

Les villes, notamment, elles emprisonnent joliment

tous les jeunes peuples de la jeunesse,

même des gitans.

elles qui appâtent avec le concret américain ou du saint bois et de la
tôle.

Même les gitans.

 

Elles se bétonnent de l'Est au Nord et du Morvan.

Je les suis, en pratiques.

Et j'ai bien cru que l'entreprise portait le nom

d'une marque du luxe,

« *Skatepark Archi-tecture* ».

</div>

<div class="text">

Qui obtiendrait ainsi un tel portrait pudique,

du tennisman en un hamster ?

Bien sûr que si !

sur son court, au prétexte qu'il se pisserait partout.

Portrait du portrait et dans une cage ? oui, tout alors.

Enfermé, pittoresque, au prétexte qu'encadrable.

 

Toujours cependant moins hamster mais peut-être moins encore que le
skateboard dans son parké,

Dans un bowl minuscule

et sur sa rampe, elle,

rafraîchie aux quatre flux de nos toupets.

 

Je se fascinerait à le voir tourner rebelle, tout de bourriques
textiles. la queue perlante, sa queue au vent, qui, elle,

elle dépasse.

Elle ferait même un peu *kinky*,

peut-être.

</div>

<div class="text">

Et ici, tout il est vrai.

on bidouille tout,

à l'exception

de mon propre engagement sur la *planxa* elle-même.

</div>

<div class="text">

Le problème avec le skateboard,

 

C'est qu'il a toujours ce côté flasque :

sur un filet, la musique ; et sur le fil, les qualités.

 

Aussi, en cas d'urgence, le skate décroche de sa combine,

il monte un groupe moins politique,

pour me jouer si fort, au fond d'un container.

 

Et une fois découvert,

une fois

puant la sueur de la sardine,

il (lui faut bien).

</div>

<div class="text">

Le Skateboard apparaît ici comme une chose matérielle, relativement
inerte, qui doit l'essentiel de sa dynamique symbolique à sa propre
masse.

</div>

<div class="text">

Le problème (*Hammerhead*) avec le skateboard,

c'est pour parler en lien d'avec mon père,

tout pour le mien.

 

Rien de plus simple que de revoir Abdel (plus que Kader), en 1986,

lorsqu'il y porte l'imperméable noir, à ce point large.

Il s'impressionne tant ce renoua-là !

Après, en 1988, comme il défonce son beau bureau avec ma *planxa* Hosoï

--- j'aimais ce *shape* dit «*hammerhead*» !

 

Abdel, mon bon papa qui s'ignorait...

 

« Vous faites quoi comme djobe », lui demande en 1987 le moustachu du
petit shop ?

 

En « 1987 » ?

Alors, par exemple, en 1988, le djobe, d'Abdelkader, c'était censeur.

il cisaillait, lui, aux ciseaux, deux de mes souliers à la pointe
arrondie.

 

C'était parce que pour lui, en français ou en arabe,

*dr martens* et *cherry red*,

ça ne signifiait que trop.

Quasiment.

 

Mais avant, en 1987, il répondait (l'air constipé) :

« je suis chromé, petit, au fond de la cour.

bref, enseignant. Commis d'office. Je ne peux pas vous recevoir.

Pas dans l'immédiat. »

</div>

<div class="text">

Là, ce soir de 1988, il me bugne, bugne, le *hammerhead*

contre son bureau --- en tellement contre,

parce que

Hosoï !

Hosoï !

Hosoï !

 

Ce soir, le corps d'Abdel espère, c'est si limpide, me le voler en
éclats.

 

Or,

que ce christian est crispant !

qu'il en appelle aux Macchabées !

Or,

que le bureau !

que le sacré du plateau paternel, il s'envole au canif !

Je l'ai bien vu, ce soir-là, le beau bureau d'Abdelkader, éducateur de
la nation et de la France,

nous jouer son retour à l'islam,

en mille senteurs de petit bois !

Je le dessine naïvement, mon père,

un sandwich cochon dans une main,

exhibant de l'autre, un traité sur le soufisme.

Il se pose là, ce malhonnête !

 

Je l'installe même, couvert de *zbeul*,

sur sa longue natte de naturiste,

pour qu'il se drague (très poliment) Mme Nataf --- elle, pourtant si nue

et comme un ver.

</div>

<div class="text">

Enfin, je l'aurai vu, moi, le bureau rebaptisé en ligne de mire par
notre Chris Hosoï,

un Californien rembourré d'*épochè*.

 

« Ceci est un corps meurtri !

Ceci... »

 

Et Abdelkader, bien sûr :

« Tu le vois, fils de con,

ce que tu me fais faire ! »

 

Et moi, fils de con :

« Oh, mais tellement, *Père* ! » Car, par ma faute, semble-t-il, Hosoï
s'est accroché

à les poèmes.

</div>

<div class="text">

Le Problème (Redressement du *Camp* et dénouement)

Abdel tance Hosoï, comme ça, jusqu'en pleine rue (Phase 1) :

« Christian, tu-n'es-qu'un *khmal*, une *brel* !

Mais t'y as pas honte... ? À écrire partout, là, sur les bleuets, comme
une... tantouze !

Tu te prends pour qui ? Une poétesse californiste ?

Ahhhh, « Mââggie-Nêêlson » !

Espèce de tarlouze, va !

*Ya khoya*, tu m'dégoûtes ! *Karba* ! Pouah (et il crache par terre) ! »

</div>

<div class="text">

Phase 2 : Après lui avoir bien foutu la honte devant des gens,

mon père compte le redresser, à-ce-Christian-Ho-soï.

Le tenant par l'oreille la plus droite, d'un coup de *hammerhead*, il le
projette au plus bas de sa cause.

 

Chris est à demi-gisant.

</div>

<div class="text">

C'est alors que pim !, Abdelkader lui administre un deuxième taquet,
qui, cette fois, envoie Hosoï très très haut dans les airs.

 

«*Hâ-kdek*, voilà ! Main-te-nant, je veux te voir faire le *stal'fish*,
hein, t'y'as compris ? »

Au cours de cette série d'échanges réparateurs, un problème *sick* avec
la sauce de l'Histoire est demeuré sous le boisseau :

Abdel, rappelons-le, reste un marxiste orthodoxe. Il n'entend rien, par
exemple, au *feedback* des idées sur leur *unterbau*.

Sa pensée est marketée en surtout « *bottom-up* ».

 

Or, du gestus au geste, un jeune *punk* américain vient d'être propulsé
dans le ciel étoilé...

!!!

</div>

<div class="text">

Vivant, idéaliste, les bras croisés en croix, Hosoï plane maintenant
*easy* au-delà du bureau.

 

Chris se réinvente d'ici en représentation,

 

comme celle du Christ !

Là où, soudain, ça fuse de la lumière. Il y a aveuglement, épiphanie et,
oui, fétiche !

et puis, puis rien.

Car, Hosoï, simplement incarné, descend tout juste, depuis sa rampe.

Moralement recomposé,

il se repeint, bien sûr.

*Skholie* : En substance, voici, bien malgré lui, (et par ma faute), que
mon père, a fait d'Hosoï un authentique *born again Christian*.

 

Il nous l'a même ordonné prêtre.

où ?

 

À *Venice* !

</div>

<div class="text">

*Résolution* : Toute la Vérité

était celle-là.

Plein de problèmes avec mon ascendance,

Comme avec la transcendance

des courbes verticales.

</div>

<div class="text">

Le problème (*camera obscura*) avec le skateboard,

C'est ma conclusion mentale.

 

Longtemps je n'ai jamais su quel usage en dériverait.

 

1. Fallait-il mourir assassiné d'un direct ou devait-on seulement se
tuer en son nom?

 

1. Cette physique de la *planxa* trouée, allions-nous l'ébruiter ?

 

Car le skate a besoin

de beaux habits nouveaux ---

mais aussi beaux que fonctionnels.

toi, alors,

bombardé de substances,

je te fais *designer*-pourquoi-pas.

 

On peut maintenant t'étourdir,

te sonner, à la différence,

et à l'identité.

</div>

<div class="text">

Comme le *sticker*, tu viens sur tout et sur tout le monde,

Ainsi va Nous !, va, de la P. L. Vivante, Nous, opacifiant les fétiches
(*Santa Independent Airlines*, reflashez, s'il vous plaît, ces aimables
QR codes !)

 

Ainsi va Nous !, va, se construire de l'image-monde,

 

Ainsi va Nous !,va, l'hallultra,

du *tot* montain.

 

Son image, belle bien et là :

Une fontaine,

alimentée à ses deux pôles.

</div>

<div class="text">

Le Problème (l'autre pôle).

Je l'ai fait croire :

avec le skate, l'autre pôle, c'est religion.

Si seulement !

 

Sans savoir les mystères, considérons ensemble cette série de systèmes :

 

1. Avant son « « *incident* » », Gator prêchait bible en mains dans les
skateparks ;

 

1. Jereme Rogers et 3. Jay Adams sont de simples convertis suintants ;

</div>

<div class="text">

4. Juché sur mon cube, j'entends clairement Hosoï, notre
pasteur-adjoint ;

 

4. J'intuite aussi le geste de Cab, bénissant humblement les Instagram ;

 

4. Combien sont-ils encore à chercher un peu de béton sacré sous le vide
de nos parkés ?

 

</div>

<div class="text">

Le Problème (l'autre pôle : *Skate and destroy!*)

L'autre pôle, avec le skateboard,

il reste mortifère.

C'est celui d'hommes rebellés, parfaitement consentants.

 

Les vieux mecs-mecs ont la saveur

de la destruction créatrice.

</div>

<div class="text">

*Skholie 1* : Le skateboard et d'intrépides martyres nous opposent des
traductions qu'ils voudraient naturelles. Seuls ou associés, ils
avancent à découvert en :

--- volume d'affaires

--- et/ou chaleur du foyer

--- et/ou chair à canoniser.

</div>

<div class="text">

*Skholie 2* : Comme des fleurs, mes propres « *Skate & destroy!* »
célèbrent peut-être :

--- mon entreprise

--- ma famille

--- et j'aime l'armée

</div>

<div class="text">

Le problème avec le skateboard :\
il s'y croit trop.

 

et c'est ses fesses,

plombées d'arrières.

</div>

<div class="text">

(le prix des *shoes* sans prix)

Le problème avec le skateboard,

C'est qu'il nous couche avec des sporcs.

</div>

<div class="text">

(Ici, j'ajouterais :

 

Ô, Olympe des crédules,

comme tu aimes à habiller

les pieds en *crap*

des forces,

à ta merci !)

</div>

<div class="text">

Le problème avec le skateboard,

Ce sont les marécageux de souliers,

baignant dans les sodas les plus littéraux.

Ce sont les estomacs des pratiquants secs,

Luttant uns à uns, contre l'étranger à soi-même,

Luttant, en sueur, pour du propre à gérer.

Or, le pouvoir les perce, il les signale

et les active,

il les sublime, à sa manière, qu'ils aimaient tant.

À Toutes surfaces d'un mêmeuh monde,

dont je ne veux pas vouloir,

Que je désire si hardiment.

À La faute de goût près.

Collé aux corpus matériels, des pieds aux têtes,

Précipitant, cool,

à la mort du public.

Anarcho-capitalo-marécageux,

Bien trop collé aux corps humains,

des pieds aux peaux, à toutes surfaces.

</div>

<div class="text">

« les patineurs deviennent sauvages dans les rues ».

Ça, c'était écrit.

</div>

<div class="text">

Le problème avec le skateboard,

C'est que c'est tout de même réservé

à des rupins.

Le prix du matos,

t'aurais qu'à voir...

Le budget de pompes à l'air qu'il te faudrait !

 

*On a eu, d'abord, l'ancêtre noblesse. Avant les codifications
bourgeoises des années 1920, et puis d'après 60.*

*Au XVII^e^, se pratiquait la skôtinnette (de « skôlé » et de
« fretin »), dans les annexes sallées des jeux de paume,*

*à l'entrée de l'hiver ou au printemps, plus tard.* *Ce sont les nobles
dans la région de Tours et du Haut Hénault, qui, malins, y initient la
cour et, oui, le roi !*

Cette histoire longue du mythe, elle fascinait mon père.

</div>

<div class="text">

Dans les années 50, en Algérie,

l'Indigénat l'avait exclu de la pratique.

Les vrais propriétaires invitaient parfois les petits Arabes à rouler du
peu dans les *half-pipe* --- que certains possédaient même en parcs
privés, perdus dans de vastes orangeraies.

 

La courbe principielle\
de la vie d'Abdel !,

Celle qui le décida à se persévérer

« professionnellement ». Par la guerre ou par l'alliance.

</div>

<div class="text">

Heureusement, ma mère, petite-fille de l'un des plus anciens *shapers*
du port d'Alger, roulait aussi dès son jeune âge,

sur des planches en bois rare --- sept plis d'ébène et d'acajou.

Elle participait aux prestigieux *protests* organisés en Métropole (dont
le grand prix du PCF).

Elle offrait souvent, aussi, sa première planche à la seconde brue de
Grace

de Monaco.

</div>

<div class="text">

Abdel et elle rêvaient de voir leur propre fils pratiquer au plus haut
des niveaux.

Ils m'avaient donc inscrit dans une équipe de patineurs,

un club très select du cinquième arrondissement --- qui développait tout
de même des *chapters* en province.

Sur un mur, à la bombe noire, ils m'avaient fait inscrire.

Et puis ils avaient repeint une partie de mon mur.

 

C'était à mon honneur. Le signe qu'à mon tour on m'initialisait.

*Skholie* : À 13 ans, comme un Hosoï-et-Cab, on me voyait déjà

de la

légende vivante !

</div>

<div class="text">

Le problème avec le skateboard,

c'est son amour du *name and shame*. Comme ça --- ou plus :

Jay Adams,

mort au front !

Jeff Grosso,

mort au front !

Jeff Philips,

mort au fond

Christian Hosoï-Prigent,

illisible !

Jay Adams

mort aux cons

Dave Martelleur,

mort aux vaches

suf marenda,

skater de mes bowls !

</div>

<div class="text">

*ut pictura*...

J'ai la nausée, je suis si froid, à me pousser du coude,

jusqu'à l'aniculaire.

J'ai deux genoux sous la main gauche,

tout près de mon index.

 

Le problème avec le skateboard,

C'est la richesse du percept.

Le gouffre de cette richesse,

qui souffre

par le bas

des imaginations.

</div>

<div class="text">

Mes yeux s'ouvrent eux sur des tous prosaïques,

muret : oui-mais ; borne : oui-non ; butoir : rugueux.

Il faudrait les avoir peints.

On en parlait, avec Denis,

de se peindre du mobilier vilain, avec la peinture rouge de l'Amérique.

L'intelligence, culturellement faible, nous faisait même

un peu défaut.

</div>

<div class="text">

Le problème avec le skateboard,

C'est évidemment,

je crois,

 

sa mère

la pure.

</div>

<div class="text">

Le problème avec le skateboard,

C'est qu'on l'expose un peu au Palais de Tokyo,

alors

qu'en vrai

ce n'est ni la danse ni le cinéma,

 

ni pas même

leur photo.

</div>

<div class="text">

Le problème avec le skateboard

est tout figuratif --- comme dans *Art Press*, lorsqu'on écrit
« figure ».

« Bien sûr que la peinture, elle est morale, s'emporte ma mère, un
foulard sur la tête !

Et la première question, pour moi, c'est *la-la*-ï-cité. »

 

Bien vu, « Maman » !

T'as toujours cette vieille gouache (30 tubes au plus), chargée
d'enluminures quasi *drop dead* ?

Des lustres pourtant que les marchands de couleurs nous les balancent
par wagons de motifs saints, par-dessous les marchés

 

Dessus, dessous, leur commerce révèle, ce faisant,

les visages ahuris

des skaters protestars.

</div>

<div class="text">

La peinture est leur croix

On peint donc sur mesure pour ceux des *Skate Nazis*,

pour les « debout les morts ! »

 

Tu dois imaginer que

chacun d'eux, il prie le Dieu, chacun de son côté

(des « face au problème », on les appelle).

Accepte-les,

organisés en coteries, se prédestinant

à jouer

les Seurat

de la planche à roulettes.

 

Alors, bien sûr, qu'avec des tâches,

certaine, qu'avec des *stains* --- presque des tâches, qui, bref aussi,
s'en retournent au *dripping* ---,

tu visionnes peut-être (enfin !) le visuel de l'Art.

Un lArge majeur,

majeur dressé,

dans son Adieu à l'Esthétique (*titanium nitride coated!*).

 

Eh bien, là, non.

Non.

De ces gargouilles ineptes, picturaves, putassières,

tu le répètes en gros,

« on voit, tu vois,

comme elles se représentent ! »

</div>

<div class="text">

Sur ta figure matoise,

se glisse, de fait, la douce plainte pataouète :

« Mais, comment donc,

des *chinese dragons* vivraient *heureux* sous les planches en érable

de "Steve Caballero" ? »

 

et ce revers salé :

« Qu'ils se contentent, ceux-là, de faire la planche,

les bras en croix,

dans le Mytheux !

*Chehhh* ! »

 

C'est là où, daronne, je te crois volontaire.

Là où tu t'en rajoutes à peine une caisse, sur ces questions féroces :

le pro fait-il la planche, autant que la planche elle fait son pro ?

Et puis de l'Artbre, à lui, qui va panser, son devenir-planchon ?

*A contrario*, pourquoi pourrir sur la souche d'un suramour de
maternelle?

Comment, enfin, La Vie rendrait-elle le *shred* plus intéressant que
l'Artbre ?

Hum ?

</div>

<div class="text">

Quelques mois avant notre brouille définitive, ma mère me débarbouille
de ce reproche glaciaire : « Tu sais que je n(e t)'aime pas

te voir là-dessus.

 

Tu sais, hein ! »

J'ai quarante ans. Pour lui rendre visite, je viens de parcourir moins
de cent mètres sur une vieille *longboard* amidonnée,

une toute blanche, sans autre motif.

De la pâle figure, oisive/inoffensive.

Le problème, donc, avec ou sans dragons,

c'est toujours au fond de ma reum,

 

Qu'elle nous hait,

 

mais graphiquement,

 

toute la Peinture !

</div>

<div class="text">

Nous aurons fait de toi

et de toute notre émeute.

C'est le problème émouvant,

 

avec le skateboard.

</div>

<div class="text">

Le problème avec du skateboard,

C'est que si je veux en dire,

 

Si je veux, ou bien m'en récrier,

 

Mes planches, mon chef, ça reste,

et du gruyère.

 

« --- On dit "mental"

--- Oui, mon chef, c'est du "mental". »

Le mental,

 

*Where definitely lies what is*

*Trou*

 

dans la planche de la langue.

</div>

<div class="text">

Le problème avec le skateboard,

C'est qu'il s'est tout de même interrogé,

 

Il s'est demandé au skateboard, ce que donnerait la mise en forme d'une
série de p*r*o*bl*èmes

sur le poème

qu'il peut susciter.

 

Pourvu, qu'on n'ait pas affaire à un « projet » !

</div>

<div class="text">

Parce que le skateboard possède bien un en-dedans,

On lui salue son *by design*.

On lui prête même

cette intériorité, réalisée à dessein,

sur une planche,

par les quinquas italiens, à col épaisse et barbes roulées, et des
montures blanchies, celles de trois jours.

 

Pourtant, perso, sa vie dedans me paraît suspecte,

parce que trop pauvre.

 

Comment, en effet, « le grand monde extérieur --- (l'autrice) veu(t)
dire tout et n'importe quoi --- est-il censé s'incorporer au monde de
l'intensité intérieure» de sept lamelles de bois d'érable
contrecollées ?

Cela ne laisse pas

d'interroger.

</div>

<div class="text">

Bien sûr que le Skateboard est un substrat !

*« Substrat ». Un terme, idéologiquement plus neutre mais à peine moins
ronflant que « matrice de subjectivation » --- en vrai, tout cela reste
moins chauve, moins lunettes, moins col roulé*

*et moins San Francisco*,

*moins lsd*,

*moins cuir*,

*moins hervéguibertmathieudindon*

*et moins mort, enfin*

*(, mec,*

*ahah !).*

</div>

<div class="text">

Le problème avec le skateboard,

C'est qu'il me donnerait presque

à applaudir.

Je lui dois, sincère, un fier calcif,

lorsqu'il

Signe de son excellente orthographe *queer*.

Monté en symbole entre deux épais virtuoses,

Il s'offre de spectacle

et puis tout à la foule.

« Ou vas-tu bidibulle, l'interroge-t-elle, reconnaissante ? »

Le roi Barbar' ne rectifie pas,

Il se contente d'une terrible précision :

« Oui je contiens du plomb,

C'est pour rester debout,

élémentaire. »

Voici enfin, une cause de déni pour ceux et tous,

Qui se circulent en forêt,

au pont d'aînesse.

</div>

<div class="text">

Le problème avec le skateboard,

c'est qu'il me fait Sujet.

Tant que je tiens dessus,

moi,

en équilibres.

</div>

<div class="text">

Le problème avec le skateboard,

Ce sont les plus intègres,

 

Qui, parmi eux, aurait misé sur quarante piges ?

Denis, là, oui.

Assez tatoué, de là à là.

Depuis toujours, dix jours de peine.

Il porte sur chaque phalange sa croix de malte, ressaisie hors commerce.

 

Elle lui bave dessus d'indépendance. Tant qu'elle le peut.

Un jour, à la gare, elle lui vaut ce chapelet d'ennuis,

avec des routards, des hommes biens,

des politiques.

Denis répond le nécessaire.

C'est un sanguin, qui aime donner

et recevoir.

 

D'autres fois pourtant, il fait son poli de miséreux.

Comme un rien, je le vois s'écraser en profil mou :

« C'est bon, c'est cool, c'est cool... c'est cool ! » Et il le répète,
ce merdeux,

tête baissée, oreille baissée, queue un peu droite (baissée).

Là, épouvantable, il SuiS celui qui craint.

</div>

<div class="text">

En 2014, on a tous nos quarante ans. Tous, plus ou moins.

On s'abonne l'un à l'autre, à nos promesses,

Ce « on » est un sincère, sans vrai calcul. Comme son tatoueur, « un
pote d'un pote qui bosse dans une mutuelle, il va me faire les *four
bars* de flat blague ».

 

Où tout en Denis s'étale en moi :

Il porte beau.

Sous ses doigts, il déplace, à lui seul l'élégance bleue,

la virulence.

Ses revers, à eux seuls, cassent parfaitement sur une peau tournée,

jaune parfaitement : sur une toile foncée,

bleue parfaitement.

 

À lui seul, il a ce goût.

Hosoï est peut-être son nom.

Où moi j'Ador(n)e le répéter.

Une, fois deux, fois

ou trois encore.

</div>

<div class="text">

Maintenant, on aperçoit ce problème.

Intermédiaire,

avec ta mère, ce chef-d'œuvre inconnu.

 

Elle ne pourrait, elle, piffrer Denis.

Non pas à cause de la longue barbe filetée de sels.

Non pas pour la netteté crasse de ses récits premiers

Non parce qu'il se prive d'alcool, de beuh et comme de tout.

Mais parce que les éboueurs l'indignent et, qu'aux tréfonds, ils
l'épouvantent.

Parce que ta mère dit toujours, d'un verbe pauvre : « C'est malheur aux
vaincus ! »

</div>

<div class="text">

Parce que *skateboard* est le stigmate de ton copain, surfer du pauvre.

Un sous-pauvre au RSA, condamné aux TIG,

Pour n'avoir su payer ses billets vers le parké de *Venice Beach*.

Non mais encore !, l'idée, encore !

Et quand vieillard, ce sera justice,

Denis crèvera dans le public, sous les hospices,

les viandards grimperont, eux, *dignitas*, dans l'énorme *jeep*,

celle de ton père,

 

à la place d'y mourir, en fanfare de klaxons.

 

Le problème avec denis, tu vois,

 

c'est qu'il n'est jamais,

 

jamais trop tôt.

</div>

<div class="text">

À Hermosa dans les Deux-Sèvres,

les mécanos ont fait main basse sur l'*air guitar*.

De leurs bleus au travail,

ils ont blondi entre les clés et les étaux.

Le temps d'une pige,

Le temps d'un hit,

je s'est offert, accompli, autorisé,

de libres exploitations de ses propres mirages.

*Révolution* : Bousculant l'ordre d'établi,

Les petits blancs,

ont stimulé

la contre-société.

</div>

<div class="text">

Théorème :

Le problème avec le skateboard,

C'est qu'il n'est pas socialiste.

</div>

<div class="text">

Le problème avec le skateboard,

C'est que je ne me laisserons pas faire.

 

Il ne me fera pas. Pas totalement.

</div>

<div class="text">

Le poème avec le skateboard,

C'est qu'aussi, désormais,

Il a tout comme un problème,

Avec moi-même.

 

Je le sais bien,

 

Et puis, quand même.

</div>

<div class="text">

switch mongo imposter (ultime cadrage ?)

« non, en vrai, c'était (au choix) :

un malfrat, notre tante, un piège, un manœuvre, une tentative d'évasion.

c'était : virjigna-ouf, de l'amour simple, un *addendum*,

c'était : de la caméra casher et un petit cœur.

c'était : un long poème.

c'était : vrai-ment tant et tant de choses, comme des *tajins*,

mais de toutes autres. »

autres, voici, enfin le tour de l'idée. car un recadrage de l'expérience
revient d'avance à invoquer cette clause : « non, en vrai, c'était... »

le « non, en vrai... », mirobolant, donc déceptif, parfois navrant. or,
c'est selon quelques enjeux.

</div>

<div class="text">

me concernant, le recadrage débutera par : « non, en vrai, j'étais...
disons... « posaïque » (je remplace).

je donnais, plutôt, pas mal dans la pose --- même si je n'ai pas
toujours été *un* posaïque. » c'est ce qui change. tout·e.

tout·e d'une vie d'humain se mêlant, en miroir, dans le rétroactif.

je sais donc à présent comme j'aurais pu me récrier :

« non, en vrai, le problème avec le skateboard, c'est bien, au final,
qu'on aura vécu si longtemps, sans se douter le moins du monde de ma
condition de posaïque (j'ai remplacé). puisque je reçois tout juste la
nouvelle --- je dois bientôt passer un test, mais tout est déjà joué,
assez. »

</div>

<div class="text">

j'écris désormais :

« mettons que pendant 45 ans, je n'étais pas malade --- puisque non, la
pose n'est pas une "maladie" ! c'est, dès lors, renversant de tant de
perspectives que d'avoir vécu une grande partie de l'existence de la
France et puis du skate "en tant que". je me serai donc traîné un peu de
tout : des aspirations, des réponses à leurs attentes --- d'autant plus
grotesques, que tant d'entre moi-même ignoraient leur posaïque
vitalité. »

 

à 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32,
33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44 ans, elle me l'avait
pourtant répété : « non, c'est juste que tu es *bizarre* ».

 

c'est plié, j'ai compris. je fais avec le bougé de l'interprétation.

j'accepte le couperet-décalé. J'écris :

 

*non, en vrai, je n'étais pas ce Croyant pour le Mytheux (celui qui
suit).*

 

*j'étais plutôt*

*ton atypique,*

*celui qui,*

*(à peine)*

</div>
