import * as THREE from 'three';
import World from './World.js';

export default class Renderer {
  constructor() {
    this.world = new World();
    this.canvas = this.world.canvas;
    this.sizes = this.world.sizes;
    this.scene = this.world.scene;
    this.camera = this.world.camera;

    this.setInstance();
  }

  setInstance() {
    this.instance = new THREE.WebGLRenderer({
      canvas: this.canvas,
      antialias: true
    });
    // this.instance.useLegacyLights = false;
    this.instance.outputColorSpace = THREE.LinearSRGBColorSpace;
    // this.instance.toneMapping = THREE.CineonToneMapping;
    this.instance.toneMapping = THREE.LinearToneMapping;
    this.instance.toneMappingExposure = 1.0;
    // this.instance.shadowMap.enabled = true;
    // this.instance.shadowMap.type = THREE.PCFSoftShadowMap;
    this.instance.setClearColor('#000000');
    this.instance.setSize(this.sizes.width, this.sizes.height);
    this.instance.setPixelRatio(Math.min(this.sizes.pixelRatio, 2));
  }

  resize() {
    this.instance.setSize(this.sizes.width, this.sizes.height);
    this.instance.setPixelRatio(Math.min(this.sizes.pixelRatio, 2));
  }

  update() {
    this.instance.render(this.scene, this.camera.instance);
  }
}
