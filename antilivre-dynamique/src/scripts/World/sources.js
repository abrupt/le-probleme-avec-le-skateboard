import * as TOOLS from '../tools/tools.js';

const nbImg = 70;
const media = [];

for (let i = 1; i <= nbImg; i++) {
  let obj;
  if (i < 10) {
    obj = {
      name: `image0${i}`,
      type: 'texture',
      path: `images/0${i}.jpg`
    };
  } else {
    obj = {
      name: `image${i}`,
      type: 'texture',
      path: `images/${i}.jpg`
    };
  }
  media.push(obj);
}

export default media;
