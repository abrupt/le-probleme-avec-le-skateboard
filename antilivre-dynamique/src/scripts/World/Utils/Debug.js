import * as dat from 'lil-gui';
import Stats from 'three/examples/jsm/libs/stats.module.js';

import World from '../World.js';

export default class Debug {
  constructor() {
    this.active = window.location.hash === '#ctrl';
    this.world = new World();
    this.canvas = this.world.canvas;

    if (this.active) {
      this.ui = new dat.GUI();
      this.stats = new Stats();
      this.canvas.parentNode.appendChild(this.stats.dom);
    }
  }

  update() {
    this.stats.update();
  }
}
