import * as THREE from 'three';
import * as SkeletonUtils from 'three/examples/jsm/utils/SkeletonUtils.js';
import World from '../World.js';
import * as TOOLS from '../../tools/tools.js';

export default class Solids extends EventTarget {
  constructor() {
    super();

    this.world = new World();
    this.scene = this.world.scene;
    this.resources = this.world.resources;
    this.time = this.world.time;
    this.debug = this.world.debug;

    this.randomPosMinY = -100;
    this.randomPosMaxY = 100;
    this.distanceMovement = 101;
    this.speedAlt = [0.05, 0.5];
    this.speedPosition = this.speedAlt[0];

    // Resource
    this.solidsList = [];

    for (let i = 1; i <= this.resources.loaded; i++) {
      if (i < 10) {
        this.setFlyingObject(this.resources.items[`image0${i}`], `0${i}`);
      } else {
        this.setFlyingObject(this.resources.items[`image${i}`], `${i}`);
      }
    }

    // Debug
    if (this.debug.active) {
      this.debugFolder = this.debug.ui.addFolder('solids');
    }
  }

  setFlyingObject(texture, nb) {
    const textureMap = texture;
    textureMap.colorSpace = THREE.LinearSRGBColorSpace;
    // textureMap.minFilter = textureMap.magFilter = THREE.LinearFilter;
    // textureMap.mapping = THREE.UVMapping;
    const geometry = new THREE.PlaneGeometry(1, 1);
    const canvas = new THREE.Group();
    const materialCanvas = new THREE.MeshBasicMaterial({
      color: 0xffffff,
      map: textureMap,
      side: THREE.FrontSide,
    });

    const meshCanvasFront = new THREE.Mesh(geometry, materialCanvas);
    const meshCanvasBack = new THREE.Mesh(geometry, materialCanvas);
    meshCanvasBack.rotation.y = Math.PI;
    meshCanvasFront.name = nb;
    meshCanvasBack.name = nb;

    canvas.add(meshCanvasFront, meshCanvasBack);

    // meshCanvas.rotation.x = -Math.PI / 2;
    canvas.scale.x = 10;
    canvas.scale.y = 10;
    canvas.position.x = TOOLS.randomNb(-100, 100);
    canvas.position.y = TOOLS.randomNb(-100, 100);
    canvas.position.z = TOOLS.randomNb(-100, 100);

    canvas.speedRotation = TOOLS.randombNbFloat(-0.2, 0.2);
    // canvas.speedPosition = TOOLS.randombNbFloat(-0.2, 0.2);
    canvas.multiplyX = TOOLS.plusOrMinus();
    canvas.multiplyY = TOOLS.plusOrMinus();
    canvas.multiplyZ = TOOLS.plusOrMinus();
    this.solidsList.push(canvas);
    this.scene.add(canvas);
  }

  update() {
    for (const solid of this.solidsList) {
      const time = this.time.elapsed / 1000;
      // solid.rotation.x = time * solid.speedRotation;
      // solid.rotation.y = time * solid.speedRotation;
      // const solidAngle = this.time.elapsed / 1000;
      // solid.position.x = Math.cos(solidAngle) * (7 + Math.sin(this.time.elapsed * 0.32));
      // solid.position.z = Math.sin(solidAngle) * (7 + Math.sin(this.time.elapsed * 0.5));
      // solid.position.y = Math.sin(this.time.elapsed * 4) + Math.sin(this.time.elapsed * 2.5);

      if (solid.position.x > this.distanceMovement) {
        solid.multiplyX = -1;
      } else if (solid.position.x < -this.distanceMovement) {
        solid.multiplyX = 1;
      }
      if (solid.position.y > this.distanceMovement) {
        solid.multiplyY = -1;
      } else if (solid.position.y < -this.distanceMovement) {
        solid.multiplyY = 1;
      }
      if (solid.position.z > this.distanceMovement) {
        solid.multiplyZ = -1;
      } else if (solid.position.z < -this.distanceMovement) {
        solid.multiplyZ = 1;
      }
      solid.position.x += (solid.multiplyX * this.speedPosition);
      solid.position.y += (solid.multiplyY * this.speedPosition);
      solid.position.z += (solid.multiplyZ * this.speedPosition);
    }
  }
}
