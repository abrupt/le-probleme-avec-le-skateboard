import * as THREE from 'three';
import { Water } from 'three/examples/jsm/objects/Water.js';
import { Sky } from 'three/examples/jsm/objects/Sky.js';
import World from '../World.js';

export default class Environment {
  constructor() {
    this.world = new World();
    this.scene = this.world.scene;
    this.renderer = this.world.renderer;
    this.resources = this.world.resources;
    this.debug = this.world.debug;

    this.sun = new THREE.Vector3();
    this.sky = new Sky();
    this.water = new Water();

    // Debug
    if (this.debug.active) {
      this.debugFolder = this.debug.ui.addFolder('environment');
    }

    this.params = {
      background: 0x000000,
      fog: 0x000000,
    };

    this.setLight();
  }

  setLight() {
    this.ambientLight = new THREE.AmbientLight('#ffffff', 1); // soft white light
    this.scene.add(this.ambientLight);
    this.sunLight = new THREE.DirectionalLight('#ffffff', 4);
    // this.sunLight.castShadow = true;
    // this.sunLight.shadow.camera.far = 15;
    // this.sunLight.shadow.mapSize.set(1024, 1024);
    // this.sunLight.shadow.normalBias = 0.05;
    this.sunLight.position.set(3.5, 2, -1.25);
    this.scene.add(this.sunLight);

    // Background
    this.scene.background = new THREE.Color(this.params.background);
    // this.scene.fog = new THREE.FogExp2(this.params.fog, 0.0025);
    // if (this.debug.active) {
    //   this.debugFolder
    //     .add(this.params, 'background', { Rose: 0xfa81cb, Bleu: 0x00ecfa })
    //     .name('Background Color')
    //     .onChange((value) => {
    //       this.scene.background.set(value);
    //       this.scene.fog.color = new THREE.Color(value);
    //       // this.scene.fog.set(value);
    //     });
    // }
  }
}
