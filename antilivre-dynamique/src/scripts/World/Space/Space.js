import World from '../World.js';
import Environment from './Environment.js';
import Solids from './Solids.js';

export default class Space extends EventTarget {
  constructor() {
    super();

    this.world = new World();
    this.scene = this.world.scene;
    this.resources = this.world.resources;

    // Wait for resources
    this.resources.addEventListener('ready', () => {
      // Setup
      this.environment = new Environment();
      this.solids = new Solids();
      this.dispatchEvent(new Event('solidsReady'));
      document.querySelector('.loading').classList.add('loading--done');
      document.querySelector('body').classList.add('pace--three-loaded');
    });
  }

  update() {
    // if (this.environment) this.environment.update();
    if (this.solids) this.solids.update();
  }
}
