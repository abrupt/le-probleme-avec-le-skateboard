import * as THREE from 'three';

import Debug from './Utils/Debug.js';
import Sizes from './Utils/Sizes.js';
import Time from './Utils/Time.js';
import Raycaster from './Utils/Raycaster.js';
import Camera from './Camera.js';
import Renderer from './Renderer.js';
import Space from './Space/Space.js';
import Resources from './Utils/Resources.js';

import sources from './sources.js';

export default class World {
  constructor(canvas) {
    if (World.instance) {
      return World.instance;
    }
    World.instance = this;

    // canvas
    this.canvas = canvas;

    // Setup
    this.debug = new Debug();
    this.sizes = new Sizes();
    this.time = new Time();
    this.scene = new THREE.Scene();
    this.resources = new Resources(sources);
    this.camera = new Camera();
    this.renderer = new Renderer();
    this.space = new Space();
    this.raycaster = new Raycaster();

    // Events
    // Resize event
    this.sizes.addEventListener('resize', () => {
      this.resize();
    });
    // Time animation event
    this.time.addEventListener('animation', () => {
      this.update();
    });
  }

  resize() {
    this.camera.resize();
    this.renderer.resize();
  }

  update() {
    this.camera.update();
    this.space.update();
    this.raycaster.update();
    if (this.debug.active) this.debug.update();
    this.renderer.update();
  }

  // destroy() {
  // }
}
