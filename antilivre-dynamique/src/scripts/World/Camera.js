import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import World from './World.js';

export default class Camera {
  constructor() {
    this.world = new World();
    this.sizes = this.world.sizes;
    this.scene = this.world.scene;
    this.canvas = this.world.canvas;

    this.movingCamera = false;
    this.movingCameraEnd = false;
    this.timer = null;

    this.setInstance();
    this.setControls();

    this.controls.addEventListener('start', () => {
      this.movingCameraEnd = false;
    });

    this.controls.addEventListener('end', () => {
      clearTimeout(this.timer);
      this.movingCameraEnd = true;
      if (this.movingCamera) {
        this.movingCamera = false;
      }
    });

    this.controls.addEventListener('change', () => {
      if (this.movingCameraEnd) {
        clearTimeout(this.timer);
        this.movingCamera = false;
      } else {
        this.timer = setTimeout(() => {
          this.movingCamera = true;
        }, 150);
      }
    });
  }

  setInstance() {
    this.instance = new THREE.PerspectiveCamera(50, this.sizes.width / this.sizes.height, 0.1, 5000);
    this.instance.position.set(0, 0, 90);
    this.scene.add(this.instance);
  }

  setControls() {
    this.controls = new OrbitControls(this.instance, this.canvas);
    this.controls.enableDamping = true;
    this.controls.dampingFactor = 0.05;
    // this.controls.panSpeed = 0.2;
    // this.controls.rotateSpeed = 0.8;
    // this.controls.zoomSpeed = 0.8;
    this.controls.minDistance = 0;
    this.controls.maxDistance = 500;
    // this.controls.maxPolarAngle = Math.PI * 0.5;
    // this.controls.target = new THREE.Vector3(0, 0.5, 0);
  }

  resize() {
    this.instance.aspect = this.sizes.width / this.sizes.height;
    this.instance.updateProjectionMatrix();
  }

  update() {
    this.controls.update();
  }
}
