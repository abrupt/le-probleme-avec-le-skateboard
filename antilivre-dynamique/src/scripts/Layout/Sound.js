import APlayer from 'aplayer';
import SimpleBar from 'simplebar';

export default class Sound {
  constructor() {
    this.musicInterface = document.querySelector('.music');
    this.soundBtn = document.querySelector('.button--sound');
    this.soundBtnOn = document.querySelector('.button--sound .button__svg--on');
    this.soundBtnOff = document.querySelector('.button--sound .button__svg--off');
    this.sound = document.querySelector('.sound--background');
    this.vol = 0;
    this.interval = 30;
    this.fadeInAudio = null;
    this.fadeOutAudio = null;
    this.silence = true;

    this.soundBtn.addEventListener('click', (e) => {
      e.preventDefault();
      this.soundBtn.blur();

      // if (this.silence) {
      //   this.play(this.sound);
      // } else {
      //   clearInterval(this.fadeInAudio);
      //   clearInterval(this.fadeOutAudio);
      //   this.sound.pause();
      // }
      this.soundBtnOn.classList.toggle('none');
      this.soundBtnOff.classList.toggle('none');
      this.musicInterface.classList.toggle('show');

      // this.silence = !this.silence;
    });

    const ap = new APlayer({
      container: document.querySelector('.music__player'),
      mini: false,
      autoplay: false,
      theme: '#ffffff',
      loop: 'all',
      order: 'list',
      preload: 'auto',
      volume: 0.7,
      mutex: true,
      listFolded: false,
      listMaxHeight: '100%',
      lrcType: 1,
      audio: [
        {
          name: 'je n’en suis pas',
          artist: 'petit bourgeois sauvage',
          url: './sounds/01.mp3',
          cover: './images/01.jpg',
        },
        {
          name: 'séminaire causé par l’animal',
          artist: 'petit bourgeois sauvage',
          url: './sounds/02.mp3',
          cover: './images/02.jpg',
        },
        {
          name: 'l’Antiquité, c’était mieux que rien',
          artist: 'petit bourgeois sauvage',
          url: './sounds/03.mp3',
          cover: './images/03.jpg',
        },
        {
          name: 'la tête au soleil',
          artist: 'petit bourgeois sauvage',
          url: './sounds/04.mp3',
          cover: './images/04.jpg',
        },
        {
          name: 'dans un drugstore',
          artist: 'petit bourgeois sauvage',
          url: './sounds/05.mp3',
          cover: './images/05.jpg',
        },
        {
          name: 'intérêts spicyfuck',
          artist: 'petit bourgeois sauvage',
          url: './sounds/06.mp3',
          cover: './images/06.jpg',
        },
        {
          name: 'dans les boîtes du maître',
          artist: 'petit bourgeois sauvage',
          url: './sounds/07.mp3',
          cover: './images/07.jpg',
        },
        {
          name: 'en classe',
          artist: 'petit bourgeois sauvage',
          url: './sounds/08.mp3',
          cover: './images/08.jpg',
        },
        {
          name: 'les parkés',
          artist: 'petit bourgeois sauvage',
          url: './sounds/09.mp3',
          cover: './images/09.jpg',
        },
        {
          name: 'suant la sardine',
          artist: 'petit bourgeois sauvage',
          url: './sounds/10.mp3',
          cover: './images/10.jpg',
        },
        {
          name: 'abdel vs christian hosoï',
          artist: 'petit bourgeois sauvage',
          url: './sounds/11.mp3',
          cover: './images/11.jpg',
        },
        {
          name: 'ma conclusion mentale',
          artist: 'petit bourgeois sauvage',
          url: './sounds/12.mp3',
          cover: './images/12.jpg',
        },
        {
          name: 'béton sacré',
          artist: 'petit bourgeois sauvage',
          url: './sounds/13.mp3',
          cover: './images/13.jpg',
        },
        {
          name: 'skate & destroy',
          artist: 'petit bourgeois sauvage',
          url: './sounds/14.mp3',
          cover: './images/14.jpg',
        },
        {
          name: 'ses fesses',
          artist: 'petit bourgeois sauvage',
          url: './sounds/15.mp3',
          cover: './images/15.jpg',
        },
        {
          name: 'Olympe des crédules',
          artist: 'petit bourgeois sauvage',
          url: './sounds/16.mp3',
          cover: './images/16.jpg',
        },
        {
          name: 'marécageux de souliers',
          artist: 'petit bourgeois sauvage',
          url: './sounds/17.mp3',
          cover: './images/17.jpg',
        },
        {
          name: 'Wild in the streets',
          artist: 'petit bourgeois sauvage',
          url: './sounds/18.mp3',
          cover: './images/18.jpg',
        },
        {
          name: 'l’orangeraie',
          artist: 'petit bourgeois sauvage',
          url: './sounds/19.mp3',
          cover: './images/19.jpg',
        },
        {
          name: 'off the wall',
          artist: 'petit bourgeois sauvage',
          url: './sounds/20.mp3',
          cover: './images/20.jpg',
        },
        {
          name: 'à me pousser du coude',
          artist: 'petit bourgeois sauvage',
          url: './sounds/21.mp3',
          cover: './images/21.jpg',
        },
        {
          name: 'La pure',
          artist: 'petit bourgeois sauvage',
          url: './sounds/22.mp3',
          cover: './images/22.jpg',
        },
        {
          name: 'tokyo',
          artist: 'petit bourgeois sauvage',
          url: './sounds/23.mp3',
          cover: './images/23.jpg',
        },
        {
          name: 'la haine de la peinture',
          artist: 'petit bourgeois sauvage',
          url: './sounds/24.mp3',
          cover: './images/24.jpg',
        },
        {
          name: 'de toute notre émeute',
          artist: 'petit bourgeois sauvage',
          url: './sounds/25.mp3',
          cover: './images/25.jpg',
        },
        {
          name: 'dans la planche de la langue',
          artist: 'petit bourgeois sauvage',
          url: './sounds/26.mp3',
          cover: './images/26.jpg',
        },
        {
          name: 'project punks fuck off',
          artist: 'petit bourgeois sauvage',
          url: './sounds/27.mp3',
          cover: './images/27.jpg',
        },
        {
          name: 'bidibulles',
          artist: 'petit bourgeois sauvage',
          url: './sounds/28.mp3',
          cover: './images/28.jpg',
        },
        {
          name: 'sujet',
          artist: 'petit bourgeois sauvage',
          url: './sounds/29.mp3',
          cover: './images/29.jpg',
        },
        {
          name: 'saint denis',
          artist: 'petit bourgeois sauvage',
          url: './sounds/30.mp3',
          cover: './images/30.jpg',
        },
        {
          name: 'air guitar',
          artist: 'petit bourgeois sauvage',
          url: './sounds/31.mp3',
          cover: './images/31.jpg',
        },
        {
          name: 'ce qu’il n’est pas',
          artist: 'petit bourgeois sauvage',
          url: './sounds/32.mp3',
          cover: './images/32.jpg',
        },
        {
          name: 'je ne me laisserons pas faire',
          artist: 'petit bourgeois sauvage',
          url: './sounds/33.mp3',
          cover: './images/33.jpg',
        },
        {
          name: 'je sais bien mais quand même',
          artist: 'petit bourgeois sauvage',
          url: './sounds/34.mp3',
          cover: './images/34.jpg',
        },
        {
          name: 'PROÏESIS',
          artist: 'petit bourgeois sauvage',
          url: './sounds/35.mp3',
          cover: './images/35.jpg',
        },
        {
          name: 'salut à touxtes',
          artist: 'petit bourgeois sauvage',
          url: './sounds/36.mp3',
          cover: './images/36.jpg',
        }
      ]
    });
    ap.lrc.hide();

    // SimpleBar for the menu
    this.listPlayer = document.querySelector('.aplayer-list');
    this.simpleBarEl = new SimpleBar(this.listPlayer);
    // Disable scroll simplebar for print
    window.addEventListener('beforeprint', () => {
      this.simpleBarEl.unMount();
    });
    window.addEventListener('afterprint', () => {
      this.simpleBarEl = new SimpleBar(this.listPlayer);
    });
  }

  play(track) {
    if (track.paused) {
      clearInterval(this.fadeInAudio);
      clearInterval(this.fadeOutAudio);
      track.volume = this.vol;
      track.play();
      this.fadeInAudio = setInterval(() => {
        if (track.volume < 0.99) {
          track.volume += 0.01;
        } else {
          clearInterval(this.fadeInAudio);
        }
      }, this.interval);
    } else {
      track.pause();
    }
  }
}
