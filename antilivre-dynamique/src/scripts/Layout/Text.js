import SimpleBar from 'simplebar';
import * as TOOLS from '../tools/tools.js';
import World from '../World/World.js';

export default class Text {
  constructor() {
    this.textOpen = false;
    this.texts = [...document.querySelectorAll('.text')];
    this.textContainer = document.querySelector('.texts');
    this.textsContent = document.querySelector('.texts__content');
    this.textsImage = document.querySelector('.texts__image');
    this.closeBtn = document.querySelector('.texts__close');

    this.world = new World();

    // SimpleBar for the menu
    this.simpleBarEl = new SimpleBar(this.textsContent);
    // Disable scroll simplebar for print
    window.addEventListener('beforeprint', () => {
      this.simpleBarEl.unMount();
    });
    window.addEventListener('afterprint', () => {
      this.simpleBarEl = new SimpleBar(this.textsContent);
    });

    this.textsContent.addEventListener('mousemove', () => {
      this.mouseInsideText = true;
    });

    this.textsContent.addEventListener('mouseenter', () => {
      this.mouseInsideText = true;
    });

    this.textsContent.addEventListener('mouseleave', () => {
      this.mouseInsideText = false;
    });

    this.closeBtn.addEventListener('click', (e) => {
      e.preventDefault();
      this.closeText();
      this.closeBtn.blur();
    });
  }

  showText(image) {
    this.world.space.solids.speedPosition = this.world.space.solids.speedAlt[1];
    this.texts.forEach((el) => {
      el.classList.remove('show');
    });
    this.textsImage.innerHTML = '';
    const imgEl = document.createElement('img');
    imgEl.src = `./images/${image}.jpg`;
    this.textContainer.classList.add('show');
    this.textsImage.appendChild(imgEl);
    // const randomText = TOOLS.randomNb(0, this.texts.length - 1);
    this.texts[parseInt(image) - 1].classList.add('show');
    this.texts[parseInt(image) - 1].scrollIntoView();
    this.textOpen = true;
  }

  closeText() {
    this.world.space.solids.speedPosition = this.world.space.solids.speedAlt[0];
    this.texts.forEach((el) => {
      el.classList.remove('show');
    });
    this.textsImage.innerHTML = '';
    this.textContainer.classList.remove('show');
    this.textOpen = false;
  }
}
