import { defineConfig, sharpImageService } from 'astro/config';
import mdx from '@astrojs/mdx';
import sitemap from '@astrojs/sitemap';
import prefetch from '@astrojs/prefetch';
import remarkUnwrapImages from 'remark-unwrap-images';
import smartypants from 'remark-smartypants';
import remarkTextr from 'remark-textr';
import remarkSuper from 'remark-super';

// Functions for remarkTextr
function pointExclamation(input) {
  return input.replace(/ !/gim, ' !');
}
function pointVirgule(input) {
  return input.replace(/ ;/gim, ' ;');
}
function pointInterrogation(input) {
  return input.replace(/ \?/gim, ' ?');
}
function deuxPoints(input) {
  return input.replace(/ :/gim, ' :');
}
function pourcentage(input) {
  return input.replace(/ %/gim, ' %');
}

// https://astro.build/config
export default defineConfig({
  site: 'https://www.antilivre.org',
  base: '/le-probleme-avec-le-skateboard',
  markdown: {
    smartypants: true,
    // drafts: true,
    remarkPlugins: [
      [smartypants, {
        dashes: 'oldschool',
        ellipses: true,
        quotes: true,
        backticks: false,
        openingQuotes: { single: '’', double: '« ' },
        closingQuotes: { single: '’', double: ' »' },
      }],
      [remarkTextr, {
        plugins: [
          pointExclamation,
          pointVirgule,
          pointInterrogation,
          deuxPoints,
          pourcentage,
        ],
        options: { locale: 'fr' },
      }],
      remarkUnwrapImages,
      remarkSuper,
    ],
    remarkRehype: { footnoteLabel: 'Notes', footnoteBackLabel: 'Retour au contenu' },
  },
  experimental: {
    assets: true,
  },
  image: {
    service: sharpImageService(),
  },
  integrations: [
    mdx({
      // drafts: true,
    }),
    sitemap(),
    prefetch(),
  ],
  compressHTML: true,
  vite: {
    optimizeDeps: {
      exclude: ['@resvg/resvg-js'],
    },
    assetsInclude: ['**/*.xml'],
    build: {
      chunkSizeWarningLimit: 1500,
      // rollupOptions: {
      //   output: {
      //     manualChunks: (id) => {
      //       if (id.includes('node_modules')) {
      //         if (id.includes('@aws-amplify')) {
      //           return 'vendor_aws';
      //         } if (id.includes('@material-ui')) {
      //           return 'vendor_mui';
      //         }
      //         return 'vendor'; // all other package goes here
      //       }
      //     },
      //   }
      // }
    }
  },
  build: {
    assets: 'assets',
  }
});
